import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(precision=3,suppress=True)

"""This is an example scipt on how to use this library in case you are stuck.
   There are many valid ways to use this library; this is just one."""

"""First, always import the things you need.  I imported numpy and matplotlib earlier because they are givens.  But here
   the rest are below.  Note this may be different if your directory is set up differently, so be careful.
   Also note I import a 'constants and unit conversions' library that does my unit conversions for me.  It is simular
   to the scipy library but I am used to my own version."""

import src.ChemicalSystems as cs
import src.Spaces as spaces
import src.GMSolvers as solv
from src.ConstantsAndUnitConversions import *

"""Next, you want to load ab-initio points.  This library was made so that you can give a chemical system and get the
   answer.  You can give parameters, or even a z-space representation of the problem too but that will not be shown.
   
   I use np.genfromtxt(), which turns a file into a numpy array. These are common arguments.  The delimiter tells you
   each entry is separated by a comma (note tabs are also common), and skip header allows you to say if the first line
   is words (like column headers, citaiton etc.).  If its more than one line, you are out of luck.
   
   You also need to make sure your data is in atomic units.  If it's not, you need to convert it.  Using atomic units
   is good practice in calculations because it ensures proper percision AND easily understandable units afterwards.
   If atomic units are new to you, please look up wikipedia."""

data=np.genfromtxt('ScannedPotentials/b2_surface.csv',delimiter=',',skip_header=True,).T
data[0]*=Ang2Bohr #Converts all my bond lengths from angstroms to bohrs.
data[1]/=1000 #Converts all energies form mHartree to Hatree.
mu=redmass(mF,mF)*amu2me

"""Then, you want to set up your chemical system.  To do this, you send it to one of the Chemical System Classes, 
   depending on the type of fit you want.  For this example, I am using the two parameter general morse potential.
   You can even use this time to print out some of the parameters of the space.
   
   After this, you pass this into spaces to convert the system into z-space.  Then you pass it to solvers."""

system=cs.GeneralMorse2Param(data[0],data[1],mu,size=1_000)
system.fit()

space=spaces.SystemGMSpace(system)

"""The choices of the solvers are really where you can 'change things'.  For this example, I will be using the usual
   laguerre method, but know that the classes were set up so the exact same architecture will give you the same
   result."""

lm=solv.LaguerreMethod(60,space)
lm.solve()

"""If you want to view results, make sure you ALWAYS convert to physical units.  Any conversions out of z-space is
   already handled."""

print(lm.getValues()*hartree2wn)

"""This works the exact same way as other solvers like the Symmetric Laguerre Method."""

slm=solv.SymmetricLaguerreMethod(60,space)
slm.solve()
print(slm.getValues()*hartree2wn)

pe=solv.PolynomialExpansion(160,space)
pe.solve()
print(pe.getValues()*hartree2wn)

"""And Even the CMDVR Method, with slight changes"""
dvr=solv.CMDVR(system)
dvr.solve()
print(dvr.getValues()*hartree2wn)

"""And As Always, you probably want to plot your answer.  To do so, it is as simple as pulling the right values from
   the objects created"""

#plt.plot(system.r/Ang2Bohr,system.v*hartree2kcalmol,c='k')
for i in range(5):
    plt.plot(system.r/Ang2Bohr,lm.getWavefunction(i)**2*100+pe.getValue(i)*hartree2kcalmol)
#plt.ylim(-5,system.de*hartree2kcalmol)
plt.show()

"""Finally, it appears that the CMDVR does not give the same answer as the other methods.  I have checked and it 
   is a result of the slow convergence of CMDVR with basis set size."""

