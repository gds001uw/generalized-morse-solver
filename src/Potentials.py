import numpy as np
import scipy.interpolate as inter

def morse(r,re,a,de):
    """
    This function gives the Morse Potential

    :param r: array of floats>0; this is the independent variable for the potential
    :param re: float>0; this is the equilibrium bond length of the potential.
    :param a: float>0; this alpha gives the broadness of the well.
    :param de: float>0; this gives the deepness of the well
    :return: array of floats>=0; this dependent variable is the potential service
    """
    return de*(1-np.exp(-a*(r-re)))**2

def generalMorse(r,re,a,eta,de):
    """
    This function is the general morse potential

    :param r: array of floats>0; this is the independent variable for the potential
    :param re: float>0; this is the equilibrium bond length of the potential.
    :param a: float>0; this alpha gives the broadness of the well.
    :param eta: float>0; this eta is gamma/alpha, and gives the form of the potential
                (lowering of dissociation and flips in repulsive wall)
    :param de: float>0; this gives the deepness of the well
    :return: array of floats>=0; this dependent variable is the potential service
    """
    dr=(r-re)
    return de*(eta*(np.exp(-2*a*dr)-np.exp(-a*dr))-np.exp(-eta*a*dr))

def morseRedCord(r,a):
    """
    This function gives the Morse Potential

    :param r: array of floats>0; this is the independent variable for the potential
    :param re: float>0; this is the equilibrium bond length of the potential.
    :param a: float>0; this alpha gives the broadness of the well.
    :param de: float>0; this gives the deepness of the well
    :return: array of floats>=0; this dependent variable is the potential service
    """
    r=r-1
    return (1-np.exp(-a*(r)))**2-1

def generalMorseRedCord(r,a,eta):
    """
    This function is the general morse potential

    :param r: array of floats>0; this is the independent variable for the potential
    :param a: float>0; this alpha gives the broadness of the well.
    :param eta: float>0; this eta is gamma/alpha, and gives the form of the potential
                (lowering of dissociation and flips in repulsive wall)
    :return: array of floats>=0; this dependent variable is the potential service
    """
    r=r-1
    return (eta*(np.exp(-2*a*r)-np.exp(-a*r))-np.exp(-eta*a*r))

def expn(r,alpha,beta,*an):
    """
    This function is a n-gaussian fit of a potential energy surface.
    It tends to produce very good fits of an ab-initio surface, especially in getting to the flat dissocaiton.
    This is the fit Klaus Reudenberg Uses

    :param r: array of floats>0; this is the independent variable for the potential
    :param alpha: float>0, a constant of the fitting that is the same for each expoential
    :param beta: float>0, a constant of fitting that changes for each exponential
    :param an: list of float, each is a constant scale for the guassians
    :return: array of floats>=0; this dependent variable is the potential service
    """
    surf=np.zeros(len(r))
    for i in range(len(an)):
        surf+=an[i]*np.exp(-alpha*(beta**i)*(r**2))
    return surf

def intersurf(r,x,y):
    """
    This nicely wraps scipy's interpolation method into an in-and-out function

    :param r: This is the resulting space for the surface. (independent variable)
    :param x: These are the bond lengths where ab-initio values are taken.
    :param y: These are the measured ab-initio values
    :return: the interpolated surface on the range of r is returned.
    """
    surf=inter.interp1d(x,y,kind='cubic')
    return surf(r)