import numpy as np

#All of these are constants I use to convert units or do maths outside of atomic units.
#Aparently Scipy also has these implemented so feel free to use them or this.
h=6.62607004E-34 # Js
c=2.99792458E8 # m/s
k=1.38064852E-23 # J/K
Cal2J = 4.184 # J/cal
mDyne2amu = 1/(1000*1e5*8.23872350e-8)
J2Hartree=2.294E17 #Hatree/J
Ang2Bohr=1.8897259886 #Bohr/Ang
amu2kg=1.66054E-27 # kg/amu
amu2me=1822.8885300626 # me/amu
s2au=2.418884236E-17 # s
Na=6.02214E23 #molecules/mol
hbar=h/2/np.pi #Js
e = 1.602176634E-19 #C
ao = 5.29177210903E-11 #m
Rh=1.09677583E7 #1/m
eo=8.85418781762039E-12 #C^2/Nm^2
c=2.998E8 #m
po=101325 #Pa
J2Ev=6.624E18 # eV/J
hbarev=hbar*J2Ev #eV s
hartree2wn=219474.63 #wn/hartree
hartree2kcalmol=627.5 #kcal/mol/hartree

#These
#amu
mH=1.00784
mD=2.014
mT=3.0160492
mB=(10.01293695*0.199+11.00930536*0.801)
mB11=11.00930536
mB10=10.01293695
mC=12.0107
mN=14.0067
mO=15.99949146221
mF=18.9984032
mCl=35.453
mBr=79.904

EnergyConverter={'kcal/mol':1/627.5,
                 'Eh':1,
                 'wn':1/219474.63,
                 'kJ/mol':1/2625.5,
                 'eV':1/27.211}
lengthConverter={'ao':1,
                 'ang':1.8897259886} #Atomic Unit/Unit
massConverter={'me':1,
               'amu':1822.8885300626,
               'kg':1822.8885300626/1.66054E-27}

def redmass(m1,m2): return m1*m2/(m1+m2)

def R2(yt,yr):
    sst=0
    m=np.mean(yt)
    for i in yt: sst+=(i-m)**2
    ssr=0
    for i in range(len(yt)):
        ssr+=(yt[i]-yr[i])**2
    return 1-ssr/sst

def chi2(w,e,f):
    num=0
    for i in range(len(e)):
        num+=w[i]*(e[i]-f[i])**2
    return num/sum(w)

def RMSD(e,f):
    result=0
    for i in range(len(e)):
        result+=(e[i]-f[i])**2
    return np.sqrt(result/len(e))

def MSE(e,f):
    result=0
    for i in range(len(e)):
        result+=abs(e[i]-f[i])
    return result/len(e)

def hbar2(massunits,energyunits,lengthunits):
    result=1 #Eh*me*ao^2
    result/=massConverter[massunits]
    result/=EnergyConverter[energyunits]
    result/=lengthConverter[lengthunits]**2
    return result
