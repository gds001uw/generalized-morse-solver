import numpy as np
import src.Potentials as pot


class GMSpace:
    """
    GMSpace is a bare-bones class that has everything you need to use the Solvers.
    It stores only mathematical information required for the barebones-mathematics.
        V=lambda^2(eta*(exp(-2x)-exp(-x))-exp(-eta*x))
    """
    def __init__(self,lam,eta,size=1_000):
        """
        The general constructor takes in the minimal parameters to pass into the solvers.
        Automatic constants are created to "return" to a real space for some problems.
        However these constants are chosen to be the same answers as in the reduced space.

        :param lam: float>0; this controls the depth of the "well".
        :param eta: float>0; this controls the tightness of the "well"
        :param size: int>0; This optional parameter gives you the size of the spacial grid,
                     It is set to 1,000 as a generous default.  Since the problem doesn't translate back into real
                     space, it is kindof pointless to use any methods that require a spacial grid.
        """
        self.lam=lam
        self.eta=eta
        self.re=1
        self.xe=1
        self.mu=1/2
        self.alpha=1
        self.r=np.linspace(0,10,size)
        self.z=2*self.lam*np.exp(self.re-self.r)
        self.pot=pot.generalMorse(self.r,self.re,self.alpha,self.eta,self.lam**2)

    def zEnergy2Energy(self, eps):
        """
        Mimics child's class to turn a Solver's energy into the original enegy of the system.
        Since this not for a real system, the constants (mu, alpha) are chosen so it just returns epsilon.

        For GenSpace, this method is useless, but required to allow substitutions betwene GenSpace and SystemSpace
        :param eps: Energy in z-space.
        :return: Energy back in real values from the original problem.
        """
        return eps*self.alpha**2/2/self.mu

class SystemGMSpace(GMSpace):
    """
    This child class inherits Gen Space, and allows one to give a chemical system and it pulls all the bits of the system
    into something that makes the space
    """
    def __init__(self,system):
        """
        The constructor sets up the probelm's space from the System's class, where ab-inito points made a potential surface.
        :param system: The chemical system is supplied here so that physical pieces can be loaded and the system can be
                       transfered to z-space.
        """
        from src.ConstantsAndUnitConversions import Ang2Bohr
        self.system=system
        self.r=system.r
        self.eta=system.eta
        self.re=1
        self.de=1
        self.mu=system.mu
        self.alpha=system.alpha
        self.xe=self.alpha
        self.hbar2=system.hbar2
        self.lambda2=2*system.mu/(self.hbar2*self.alpha**2)
        self.lam=np.sqrt(self.lambda2)
        self.z=2*self.lam*np.exp(self.alpha*(self.re-self.r))
        self.pot=pot.generalMorse(self.r,self.re,self.alpha,self.eta,self.lam**2)

    def zEnergy2Energy(self, eps):
        """
        Mimics child's class to turn a Solver's energy into the original enegy of the system.
        Since this not for a real system, the constants (mu, alpha) are chosen so it just returns epsilon.

        For GenSpace, this method is useless, but required to allow substitutions betwene GenSpace and SystemSpace
        :param eps: Energy in z-space.
        :return: Energy back in real values from the original problem.
        """
        return eps * self.hbar2*self.alpha ** 2 / 2 / self.mu