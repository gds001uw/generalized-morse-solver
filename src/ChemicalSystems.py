import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import src.Potentials as pot
import sys
from src.ConstantsAndUnitConversions import RMSD
from src.ConstantsAndUnitConversions import hbar2 as hb2

#This tolerance threshold decides if you can fix the dissocation limit or not.
#It is also the range a "fixed" parameter can change.
tol=1e-8

class ChemicalSystems:
    """
    This parent class holds the barebones required to take a set of ab-inito points, create parameters and
    make a surface.

    This parent class creates a cubic interpolated surface.  Child classes differ by different fitting functions.

    Note: YOU SHOULD BE GIVING EVERYTHING IN ATOMIC UNITS
          Because you are doing math, keeping things in atomic units ensures a answer in atomic units.  TRANSLATE
          EVERYTHING BEFORE AND AFTER
    """
    def __init__(self, rs, vs, mu,size=300,lengthunits='ao',energyunits='Eh',massunits='me',low=0.5,high=5):
        """
        Constructor takes in the ab-initio points and the g-matrix element of the motion.
        :param rs: array of floats; bond lengths at which ab-initio points are taken
        :param vs: arrray of floats; energy
        :param mu: float; mass of the motion
        :param size: optional parameter to give how many points should be on the surface.
        """

        amin=np.argmin(vs)
        self.re=rs[amin]
        self.rs=rs/self.re
        self.mu=mu
        self.vs=vs-vs[-1]
        self.de=-self.vs[amin]
        self.vs/=self.de
        self.size=size
        self.aguess=np.sqrt(inevenCentralDiffd2(self.rs[amin-1:amin+2],self.vs[amin-1:amin+2])/2)
        self.r = np.linspace(low,high,self.size)
        self.hbar2=hb2(massunits,energyunits,lengthunits)/self.de/self.re**2


    def fit(self):
        """
        Wrapped function that creates the finer potential surface.  This one is done fully by cubic interpolation.
        """
        self.r = np.linspace(min(self.rs),max(self.rs[:-1]),self.size)
        self.v = pot.intersurf(self.r,self.rs[:-1],self.vs[:-1])
        shift=min(self.v)
        self.v-=shift+1
        self.vs-=shift+1

    def getR(self):
        return self.r*self.re
    def getV(self):
        return self.v*self.de+self.de
    def convertToRealEnergy(self,e):
        return e*self.de

class ExpFits(ChemicalSystems):
    """
    This child class modifies fit() to fit the ab-initio points with a highly accurate gaussian method.
    Do note that the literature alpha, beta, and coefs are sometimes NOT for atomic units, so be careful.
    """
    def give(self, a, b, cs):
        a*=self.re**2
        cs/=self.de
        self.v=self.v=pot.expn(self.r,a,b,*cs)
        shift=min(self.v)
        self.v-=shift+1
        self.vs-=shift+1
        self.params=[a,b,*cs]

    def givefit(self, a, b, cs):
        a*=self.re**2
        cs/=self.de
        self.params,errors=opt.curve_fit(pot.expn,self.rs,self.vs,
                                  p0=[a,b,*cs])
        self.v=pot.expn(self.r,a,b,*cs)
        shift=min(self.v)
        self.v-=shift+1
        self.vs-=shift+1

    def fit(self,numExp):
        """
        This fit wraps the potential fitting of pot.expn() into a universaly callable function.
        Note that this fitting is VERY sensative to the input argument so be careful, play around, or check the literature.

        :param alpha: float; alpha from pot.expn()
        :param beta: float; beta from pot.expn()
        :param coefs: list of floats; coefficients for each gaussian in pot.expn()
        """
        params=np.array([1,1,0,0])
        while len(params[2:])<=numExp:
            params=np.concatenate((params,[0]))
            params, errors = opt.curve_fit(pot.expn, self.rs, self.vs,
                                       p0=[params[0],params[1],*params[2:]])
        self.params=params
        self.v=pot.expn(self.r,params[0],params[1],*params[2:])
        shift=min(self.v)
        self.v-=shift+1
        self.vs-=shift+1

class Morse1Param(ChemicalSystems):
    """
    This child class modifies fit() to fit the ab-initio points with a paritallty flexible morse potential.
    """
    def fit(self):
        """
        Fit uses the initial guesses of the parent constructor to try to fit the morse potential.  ONLY alpha is free.

        This is the prefered Morse fit if you have a good scan that can properly guess re and de.

        Note that we do check if you hit the dissociative limit.
        """
        params,errors=opt.curve_fit(pot.morseRedCord, self.rs,self.vs,
                                    p0=[self.aguess],
                                    bounds=([0],[10]))
        self.alpha=params[0]
        self.eta=1
        self.v=pot.morseRedCord(self.r,self.alpha)


class GeneralMorse2Param(ChemicalSystems):
    """
    This child class modifies fit() to fit the ab-initio points with a paritally flexible general morse potential.
    """

    def fit(self):
        """
        Fit uses the initial guesses of the parent constructor to try to fit the general morse potential.
        TWO parameters are free to move.

        This is the prefered General Morse fit if you have a good scan that can properly guess re and de.

        Note that we do check if you hit the dissociative limit.
        """
        params1, errors = opt.curve_fit(pot.generalMorseRedCord, self.rs, self.vs,
                                p0=[self.aguess, 0.75, ],
                                bounds=([0, 0], [10, 2]))
        params2, errors = opt.curve_fit(pot.generalMorseRedCord, self.rs, self.vs,
                                        p0=[self.aguess, 2.25],
                                        bounds=([0,2],[10,2.5]))
        c1=RMSD(self.vs,pot.generalMorseRedCord(self.rs,*params1))
        c2=RMSD(self.vs,pot.generalMorseRedCord(self.rs,*params2))
        if c1<c2:
            self.alpha=params1[0]
            self.eta=params1[1]
            self.error=c1
        else:
            self.alpha = params2[0]
            self.eta = params2[1]
            self.error=c2
        self.v=pot.generalMorseRedCord(self.r,self.alpha,self.eta)
        amax=np.argmax(self.v)
        if self.r[amax]<self.re:
            self.r=self.r[amax:]
            self.v=self.v[amax:]

def inevenCentralDiffd2(x,y):
    """
    This method estimates the second derivative by a 3 point central difference, allowing for the spacing to be different.

    :param x: three independent points
    :param y: three dependent points
    :return: returns a VERY rough guess of the second derivative.
    """
    a=(y[1]-y[0])/(x[1]-x[0])
    b=(y[2]-y[1])/(x[2]-x[1])
    return 2*(b-a)/(x[2]-x[0])

if __name__=='__main__':
    from src.ConstantsAndUnitConversions import *

    f2=np.genfromtxt('../ScannedPotentials/f2_surface.csv',delimiter=',',skip_header=True).T
    o2=np.genfromtxt('../ScannedPotentials/o2_surface.csv',delimiter=',',skip_header=True).T
    b2=np.genfromtxt('../ScannedPotentials/b2_surface.csv',delimiter=',',skip_header=True).T
    gmf2=GeneralMorse2Param(f2[0],f2[1],redmass(mF,mF)*amu2me)
    gmo2=GeneralMorse2Param(o2[0],o2[1],redmass(mO,mO)*amu2me)
    gmb2=GeneralMorse2Param(b2[0],b2[1],redmass(mB,mB)*amu2me)
    gmf2.fit()
    gmo2.fit()
    gmb2.fit()

    plt.plot(gmf2.r,gmf2.v,label='')
    plt.plot(gmo2.r,gmo2.v)
    plt.plot(gmb2.r,gmb2.v)
    plt.show()
