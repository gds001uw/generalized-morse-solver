from scipy.special import laguerre
from scipy.special import legendre
import scipy.linalg as lg
import src.LaguerreIntegrals as lai
import src.LegendreIntegrals as lei
from src.Spaces import *

class Solvers:
    """
    This parent class holds the very general constructor, that is soemtimes over-written.  It also includes any methods that
    need to be implemented to interprentation, so that all inherited classes have such methods possible.

    These classes mostly take in potential spaces and pulls out any of the important
    """
    def __init__(self,zspace,size=None):
        """
        The constructor takes in some important parameters to set up the mathematics barebones of the problem.
        Some parameters are copied into this class for ease and reabability, others are left in saving the Space.

        :param size: int>0; this size is the number of "basis function" in your basis set to solve the problem.
                     a good choice of size is VERY dependent on the child class.
        :param zspace: Space; This takes in a class in the Space inheritance branch that holds most of the important
                       parameters to solve these problems.
        """
        self.zspace=zspace
        if size==None: self.size=int((zspace.lam)*2)
        else: self.size=size
        self.space=zspace
        self.lam=zspace.lam
        self.eta=zspace.eta
        self.mat=np.zeros((self.size,self.size),dtype=np.float64)
        self.val=None
        self.vec=None

    def getValues(self,max=None):
        """
        This method returns a list of the eigenvalues up to some quantum number.

        :param max: int>=0; The maximum eigenvalue to be returned
        :return: list of floats; these are the lowest "max" eigenvalues.
        """
        if max==None: return self.zspace.system.convertToRealEnergy(self.zspace.zEnergy2Energy(self.val[self.val<0]))+self.zspace.system.de
        if max>self.size:
            temp=self.zspace.system.convertToRealEnergy(self.zspace.zEnergy2Energy(self.val))+self.zspace.system.de
            return np.concatenate((temp,np.zeros(max-self.size-1)))
        return self.zspace.system.convertToRealEnergy(self.zspace.zEnergy2Energy(self.val[0:max]))+self.zspace.system.de
    def getValue(self,value):
        """
        This retuns just one eigenvalue at the "value" state.
        :param value: int>=0; this is the state to be returned
        :return: returns the eigenvalues, converted back to the space's answers.
        """
        return self.zspace.system.convertToRealEnergy(self.zspace.zEnergy2Energy(self.val[value]))+self.zspace.system.de

    def getWavefunction(self,value):
        """
        This function is given so that all child classes have this and codes don't break, even if it's not implemented.
        Note that it uses the child function basisfunction() to sum up the eigenfunctions.

        :param value: int>=0; takes in the quantum number of the eignestate
        :return: returns the eigenstgate.
        """
        wfc = self.vec[value]
        wf = np.zeros(len(self.zspace.z))
        for i in range(len(wfc)):
            wf += wfc[i] * self.basisFunction(i, self.zspace.z)
        return wf
    def getWavefunctionAt(self,value,z):
        wfc=self.vec[value]
        result=0
        for i in range(len(wfc)):
            result+=wfc[i]*self.basisFunction(i,z)
        return result

    def purgeBadVals(self,tol=1):
        keep = []
        for i in range(len(self.val)):
            if self.val[i]>0:
                keep.append(False)
                continue
            temp = abs(self.getWavefunctionAt(i,2*self.lam*np.exp(-10)))
            if temp > tol:
                print(temp)
                print(self.val[i])
                keep.append(False)
            else:
                keep.append(True)
        self.val = self.val[keep]
        self.vec = self.vec[keep]

class LaguerreMethod(Solvers):
    """
    This child class solves the general morse potential through a simple laguerre polynomial basis set.
    """
    def solve(self):
        """
        The Solve method sets up the integrations to get the matrix S.  It diagonalizes S to get eigenvalues and
        eigenvectors.  It also orders them and casts everythign to real values.
        """
        self.limr=lai.LagIntMatRep(self.size)
        self.mat=-self.limr.integralMat(self.lam,self.eta)
        self.val,self.vec=lg.eig(self.mat)
        order=np.argsort(np.real(self.val))
        self.vec=self.vec.T
        self.val=np.real(self.val[order])
        self.vec=np.real(self.vec[order])


    def basisFunction(self,value,z):
        """
        This is a representation of the basis functions that self.vec has coefficients for.  It is ONLY called by
        super.getWavefunction() to sum to the total eigenstate.
        :param value: asks for the eigenstate number
        :param z: this is the z-space z range that
        :return: it returns the basis function on the z-range
        """
        return laguerre(value)(z)*np.exp(-z/2)


class SymmetricLaguerreMethod(Solvers):
    """
    This child class solves the general morse potential through a symmetrized laguerre polynomial basis set.
    It changes the sandwitch operator to ensure that the matrix is hermitian and the eigenvalues are real.
    """
    def solve(self):
        """
        The Solve method sets up the integrations to get the matrix S.  It diagonalizes S to get eigenvalues and
        eigenvectors.  It also orders each the values and vectors.
        """
        self.limr=lai.LagIntMatRep(self.size)
        self.mat=self.limr.integralMatSym(self.lam,self.eta)
        self.val,self.vec=lg.eigh(-self.mat)
        order=np.argsort(self.val)
        self.val=self.val[order]
        self.vec=self.vec[order]
        self.vec=self.vec.T


    def basisFucntion(self,value,z):
        """
        This is a representation of the basis functions that self.vec has coefficients for.  It is ONLY called by
        super.getWavefunction() to sum to the total eigenstate.
        :param value: asks for the eigenstate number
        :param z: this is the z-space z range that
        :return: it returns the basis function on the z-range
        """
        return laguerre(value)(z)*np.exp(-z/2)*z**(1/2)

class PolynomialExpansion(Solvers):
    """
    This child class is very different and uses Legrange polynomials as the basis set to create the eigenvalue problem.
    This has the advantage of being integrated over a smaller range.

    Because of it's difference, the constructor is re-defined to set up the bounds.
    """
    def __init__(self,size,zspace,bound=None):
        """
        The new constructor stars with the old constructor to save the important pieces of z-space.
        But also then adds the parameter of zmax which is the the translation of r=0 => zmax=2*lam*exp(xmin-xe).

        :param size: int>0; this is the number of basis functions.
        :param zspace: Space, holds most of the important parameters
        :param bound: This allows someone to "get"
        """
        super().__init__(size,zspace)
        if bound==None: self.zmax=2*self.lam*np.exp(zspace.alpha*(zspace.r[0]-zspace.re))
        else: self.zmax=2*self.lam*np.exp(bound)

    def solve(self):
        """
        The Solve Method sets up the Lengrange Basis Set Representation.  It then diagonalizes the matrix,
        and orders the answers.
        """
        self.limr=lei.LegIntMatRep(0,self.zmax,self.size) #This Step is GOOD!
        self.mat,self.T=self.limr.integrateMats(self.lam,self.eta) #This Step Throws Errors
        self.val,self.vec=lg.eigh(-self.mat,b=self.T)
        order=np.argsort(self.val)
        self.val = self.val[order]
        self.vec = self.vec[order]
        self.vec = self.vec.T

    def basisFunction(self,value,z):
        """
        I don't know what the basis functions should be, so lm cry and just return zero.
        :param value: asks for the eigenstate number
        :param z: this is the z-space z range that
        :return: it returns 0's because this is a bad method to begin with.
        """
        xs=np.linspace(-1,1,len(z))
        if value%2==0:
            return (legendre(value)(z)-np.sqrt(2*value+1)*legendre(0)(z))/z
        else:
            return (legendre(value)(z)-np.sqrt((2*value+1)/3)*legendre(1)(z))/z


class CMDVR:
    """
    This class is NOT a child like the prior classes since everything is so different.  Since it acts in real space,
    the Chemical system is loaded, NOT the space.  This is VERY different.
    """
    def __init__(self,system):
        """
        The new constructor now saves pieces of r-space that is important to DVR, such as the potenial curve, r-range etc.
        :param rspace: Space; this holds all the information of the space.  It is used to pull out important pieces.
        """
        from src.ConstantsAndUnitConversions import Ang2Bohr

        self.system=system
        self.m=len(self.system.v)
        self.r=system.r
        self.mu=system.mu
        self.pot=system.v
        self.hbar2=system.hbar2

    def solve(self):
        """
        The solve method is much more involved now. It sets up the very easy potential matrix, and the less easy kinetic
        matrix.  It uses the range (-infty,infty) because it works best.

        It sets up the two physical matrices, adds them, and diagonalizes the rsulting hamiltonian.  That makes the
        eigenvalues (energy states) and eigenstates (wavefunctions) that are ordered.
        :return:
        """
        V=np.diag(self.pot)
        dr=self.r[1]-self.r[0]
        K=np.zeros([self.m,self.m])
        for i in range(self.m):
            for j in range(self.m):
                if i==j: K[i][j]=np.pi**2/3
                else: K[i][j]=(-1)**(i-j)*(2/(i-j)**2)
        K/=2*self.mu*dr**2/self.hbar2
        self.val,self.vec=lg.eigh(V+K)
        order=np.argsort(self.val)
        self.val=self.val[order]
        self.vec=self.vec[order]
        self.vec=self.vec.T

    def getValues(self,max=None):
        """
        This method returns a list of the eigenvalues up to some quantum number.

        :param max: int>=0; The maximum eigenvalue to be returned
        :return: list of floats; these are the lowest "max" eigenvalues.
        """
        if max==None: return self.val[self.val<0]*self.system.de+self.system.de
        return self.val[:max]*self.system.de+self.system.de
    def getValue(self,value):
        """
        This retuns just one eigenvalue at the "value" state.
        :param value: int>=0; this is the state to be returned
        :return: returns the eigenvalues, converted back to the space's answers.
        """
        return self.val[value]

    def getWavefunction(self,value):
        """
        The wavefunctions from DVR are really simple.  Because the basis set are delta functions on a grid, each coefficnet
        is the value of the wavefunction at that gridpoint.
        :param value: int>=0; this is the eigenstate being requested
        :return: returns the wavefuncitons.
        """
        return self.vec[value]

def analyticMorse(lam):
    nmax=np.floor(lam-0.5)
    result=[]
    for i in range(int(nmax)):
        result.append((lam-0.5-i)**2)
    return -np.array(result)