import numpy as np
import scipy.linalg as lg

class LegIntMatRep:
    """

    """
    def __init__(self,lower,upper,M,N=2**8):
        """

        :param lower:
        :param upper:
        :param M:
        :param N:
        """
        self.min=lower
        self.max=upper
        self.M=M
        self.Nq=N
        self.Np=2**8
        A=np.zeros((self.Nq,self.Nq))
        for i in range(1,self.Nq):
            temp=i*i/(4*i*i-1)
            A[i,i-1]=np.sqrt(temp)
            A[i-1,i]=np.sqrt(temp)
        self.xvG,vec=lg.eigh(A)
        W=vec[0].T
        W=2*W**2
        W*=(self.max-self.min)/2
        self.W=np.diag(W)#Good Up To Here
        self.legG=self.LegGen(self.xvG)*np.sqrt((2/(self.max-self.min))) #This one is good!
        self.legDG=self.LegGenD()*(2/(self.max-self.min))**1.5 #This one was fixed!
        self.QG=self.legG[:][2:]
        self.QDG=self.legDG[:][2:]
        for i in range(self.M-1):
            if i%2==0:
                temp=(np.sqrt(2*(i+2)+1))*self.legG[:][0]
                self.QG[i][:]-=temp
            else:
                self.QG[i][:]-=np.sqrt((2*(i+2)+1)/3)*self.legG[:][1]
                self.QDG[i][:]-=np.sqrt((2*(i+2)+1)/3)*self.legDG[:][1]
        print("done")
        self.QDG=self.QDG.T
        self.QG=self.QG.T

    def integrateMats(self,lam,eta):
        """

        :param lam:
        :param eta:
        :return:
        """
        xvG=(self.xvG+1)*(self.max-self.min)/2+self.min
        td=mulElemVecMat(xvG,self.QDG)
        Sa=np.dot(np.dot(self.QDG.T,self.W),td)
        temp=-eta/4*xvG**2+lam*eta/2*xvG+(lam**(2-eta)/2**eta)*xvG**eta
        ttemp=mulElemVecMat(temp,self.QG)
        tdt=divElemVecMat(xvG,ttemp)
        Sb=np.dot(np.dot(self.QG.T,self.W),tdt)
        S=Sb-Sa
        dt=divElemVecMat(xvG,self.QG)
        T=np.dot(np.dot(self.QG.T,self.W),dt)
        return S, T

    def LegGen(self,x):
        """

        :param x:
        :return:
        """
        n=len(x)
        legG=np.zeros((self.M+1,n))
        legG[:][0]=np.ones((n))/np.sqrt(2)
        legG[:][1]=x*np.sqrt(3/2)
        for j in range(2,self.M+1):
            legG[:][j]=np.sqrt((2*j+1)*(2*j-1))/(j)*x*legG[:][j-1]
            legG[:][j]-=(j-1)/(j)*np.sqrt((2*j+1)/(2*j-3))*legG[:][j-2]
        return legG
    def LegGenD(self):
        """

        :return:
        """
        legDG=np.zeros((self.M+1,self.Nq))
        xvC=-np.cos(np.pi*np.array(range(0,self.Np))/(self.Np-1))#Right Answer
        legtmp=self.LegGen(xvC).T #Right Answer
        for i in range(self.M):
            lbleh=legtmp.T[:][i] #This works!
            ibleh=self.ChebInv(lbleh)
            tmp=self.ChebDeriv(ibleh)
            legDG[:][i]=self.ChebEval(tmp,self.xvG)
        return legDG
    def ChebDeriv(self,v):
        """

        :param v:
        :return:
        """
        l=len(v)
        D=np.zeros(np.shape(v))
        D[l-2]=(l-1)*v[l-1]
        for j in range(l-2,0,-1):
            D[j-1]=j*v[j]+D[j+1]
        D[1:]=2*D[1:]
        return D
    def ChebInv(self,v):
        """

        :param v:
        :return:
        """
        W=np.flip(v)
        W=np.concatenate((W,v[1:-1]))
        W=np.fft.ifft(W)
        C=np.real(W[0:len(v)])
        C[1:-1]=2*C[1:-1]
        return C
    def ChebEval(self,v,xx):
        """

        :param v:
        :param xx:
        :return:
        """
        l=len(xx)
        n=len(v)
        E=np.zeros(l)
        for j in range(l):
            x=xx[j]
            b1=v[n-1]
            b0=2*x*b1+v[n-2]
            for i in range(n-2,0,-1):
                tmp=2*x*b0+v[i]-b1
                b1=b0
                b0=tmp
            E[j]=x*b0+v[0]-b1
        return E

def mulElemVecMat(vec,mat):
    """

    :param vec:
    :param mat:
    :return:
    """
    result=np.zeros(np.shape(mat))
    for i in range(len(vec)):
        result[:][i]=vec[i]*mat[i]
    return result

def divElemVecMat(vec,mat):
    """

    :param vec:
    :param mat:
    :return:
    """
    result=np.zeros(np.shape(mat))
    for i in range(len(vec)):
        result[i][:]=mat[i]/vec[i]
    return result