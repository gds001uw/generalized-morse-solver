import numpy as np
from scipy.special import laguerre
import scipy.linalg as lg
import scipy.integrate as inte
import scipy.sparse as sp
import time

class LagIntMatRep:
    """
    This class creates the matrix representation of the Laguerre Basis Set.
    Note this classes forces you to use the usual Laguerre polynomials of order 0
    (y-intercpet=1), which is the best choice for these problems.
    """
    def __init__(self,M):
        """
        Initialization sets up most all of the basis set representaiton.

        :param M: Int, Number of Laguerre basis functions to use.
                  This should be about twice the number of accessible states
        """
        self.N=M
        self.M=M
        A=np.zeros((self.N,self.N))
        for i in range(self.N-1):
            A[i][i]=2*i+1
            A[i][i+1]=i+1
            A[i+1][i]=i+1
        A[self.N-1][self.N-1]=2*self.N-1
        self.xvG,vec=lg.eigh(A)
        wts=(vec[self.N-1]/self.LagGen0())**2
        self.W=np.diag(wts)
        self.lagG=self.LagGen()

    def integralMat(self,lam,eta):
        """
        This method takes the pre-set up Laguerre basis set and computes the Matrix
        That satisfies S_lj=<Ll|(1-eta)/4*z**2+(lambda*eta-1-2*j)/2*z+lambda^(2-eta)/2^eta*z**(eta)|Lj>0
        where <l||j>0=int_0^infty l*j*exp(-z)dz

        :param lam: float>0; this comes from the physical set-up of the problem.
        :param eta: float>0; this comes from the extra bit to the general morse potential.
        :return: Matrix S that has elements from the predefined integrals.
        """
        t=mulElemVecMat(self.xvG,self.lagG)
        S_eta=np.dot(np.dot(self.lagG.T,self.W),mulElemVecMat(self.xvG**eta,self.lagG))
        S_eta*=lam**(2-eta)/2**eta
        S_2=np.dot(np.dot(t.T,self.W),t)
        S_2*=(1-eta)/4
        S_1=np.dot(np.dot(self.lagG.T,self.W),((lam*eta-1-2*np.array(range(0,self.M+1)))/2*t))
        return S_eta+S_2+S_1

    def integralMatReduced(self,lam,eta):
        """
        This method takes the pre-set up Laguerre basis set and computes the Matrix
        That satisfies S_lj=<Ll|(1-eta)/4*z**2+(lambda*eta-1-2*j)/2*z+lambda^(2-eta)/2^eta*z**(eta)|Lj>0
        where <l||j>0=int_0^infty l*j*exp(-z)dz

        :param lam: float>0; this comes from the physical set-up of the problem.
        :param eta: float>0; this comes from the extra bit to the general morse potential.
        :return: Matrix S that has elements from the predefined integrals.
        """
        t=mulElemVecMat(self.xvG,self.lagG)
        S_eta=np.dot(np.dot(self.lagG.T,self.W),mulElemVecMat(self.xvG**eta,self.lagG))
        S_eta*=lam**(2-eta)/2**eta
        S_2=np.dot(np.dot(t.T,self.W),t)
        S_2*=(1-eta)/4
        S_1=np.dot(np.dot(self.lagG.T,self.W),((lam*eta-1-2*np.array(range(0,self.M+1)))/2*t))
        return S_eta+S_2+S_1

    def integralMatSym(self,lam,eta):
        """
        This method takes the pre-set up Laguerre basis set and computes the matrix elements described
        later.  Note that this method requires that beta>=1, but here we make beta=1, so that if we use
        nice Laguerre polynomials of order 0 make the leftover matrix T=I and the eigenvalue
        problem is more basic.

        The matrix elements can be defined as follows.
        #TODO Define the Matrix Elements!

        :param lam: float>0; this comes from the physical set-up of the problem.
        :param eta: float>0; this comes from the extra bit to the general morse potential.
        :return: Matrix S that has elements from the predefined integrals.
        """
        beta=1
        self.lagG1=self.LagGen1()
        temp1=(self.lagG1 * np.sqrt(np.array(range(1, self.M + 1))).T)
        lagDG=np.zeros((self.N,self.M+1))
        for i in range(self.N):
            for j in range(1,self.M):
                lagDG[i][j]=-temp1[i][j-1]

        t0=mulElemVecMat(self.xvG,self.lagG)
        t1=mulElemVecMat(self.xvG,lagDG)
        tt1=mulElemVecMat(self.xvG**2,lagDG)

        Sa=np.dot(np.dot(t0.T,self.W),t0)/4
        Sb=np.dot(np.dot(self.lagG.T,self.W),self.lagG)*1/4
        Sc=np.dot(np.dot(t1.T,self.W),t1)
        Sd=np.dot(np.dot(self.lagG.T,self.W),t0)*beta/2
        Se=np.dot(np.dot(self.lagG.T,self.W),tt1)/2
        Sf=np.dot(np.dot(tt1.T,self.W),self.lagG)/2
        Sg=np.dot(np.dot(self.lagG.T,self.W),t1)*beta/2
        Sh=np.dot(np.dot(t1.T,self.W),self.lagG)*beta/2
        S1=Sa+Sb+Sc-Sd-Se-Sf+Sg+Sh

        temp=-eta/4*self.xvG**2+lam*eta/2*self.xvG+lam**(2-eta)/2**eta*self.xvG**eta
        tlist=mulElemVecMat(temp,self.lagG)
        S2=np.dot(np.dot(self.lagG.T,self.W),tlist)
        S=S2-S1
        return S

    def LagGen0(self):
        """
        This method is only callable in the constructor. It creates the the object LagG0,
        which is required to scale the items in W appropriately.
        #TODO Figure out what this LagG0 is and why we need it!
        :return: The LagG0
        """
        N=self.N
        gafactor=1
        Lag0=1/np.sqrt(gafactor)*np.ones(len(self.xvG))
        Lag1=(1-self.xvG)/np.sqrt(1)
        for j in range(2,N):
            Lag=1/j*((2*j-1-self.xvG)*Lag1-(j-1)*Lag0)
            Lag0=Lag1
            Lag1=Lag
        return Lag1
    def LagGen(self):
        """
        This method is only callable by the constructor.  It creates the Laguerre basis set,
        LagG.
        :return:
        """
        gf=1
        lag=np.zeros((self.M+1,self.N))
        lag[:][0]=1/np.sqrt(gf)
        lag[:][1]=(1-self.xvG)
        for j in range(2,self.M):
            lag[j]=1/j*((2*j-1-self.xvG)*lag[:][j-1]-(j-1)*lag[:][j-2])
        return lag.T
    def LagGen1(self):
        """
        This method is only callable by integralMatSym().  It creates a LagG matrix where alpha is 1,
        not zero.  This is used only once to create the LagGD matrix.
        :return: the LagG1 matrix.
        """
        gf = 1
        lag = np.zeros((self.M, self.N))
        lag[:][0] = 1 / np.sqrt(gf)
        lag[:][1] = (2 - self.xvG) / np.sqrt(gf * (2))
        fac0 = 1 / np.sqrt(2)
        for j in range(2, self.M):
            fac1 = np.sqrt(j / (j + 1))
            lag[:][j] = fac1 / j * ((2 * j - self.xvG) * lag[:][j - 1] - (j) * fac0 * lag[:][j - 2])
            fac0 = fac1
        return lag.T

def mulElemVecMat(vec,mat):
    """
    This method multiplies a matrix and vector like matlab does, but numpy doesn't allow.
    :param vec: A vector of floats.
    :param mat: A matrix of floats.
                The number of rows must be equal to the length of the vector!
    :return: Returns a matrix of the same dimentions of mat.
    """
    result=np.zeros(np.shape(mat))
    for i in range(len(vec)):
        result[i][:]=vec[i]*mat[i]
    return result

def integralint(l,j,n):
    """
    This quickly computes the integrals with laguerre polymomials that exploit
    the orthoganality, and forces the recursive relationship to orthoganol terms.

    :param l: int>=0; Order of one Laguerre Polynomial
    :param j: int >=0; Order of the other Laguerre Polynimial
    :param n: int; exponent of any additional term left in the integral.
    :return: returns int_0^infty Ll*Lj*exp(-z)*z^n dz
    """
    if l>j:
        temp=j
        j=l
        l=temp
    if l+n<j: return 0
    if n==0: return 1
    return (2*l+1)*integralint(l,j,n-1)-(l+1)*integralint(l+1,j,n-1)-l*integralint(l-1,j,n-1)

def integral(l,j,eta):
    """
    This wraps the quad integral method to solve integrals of laguerre polynomials
    that are multiplied by a non-integer exponent.  Note this method is SLOW and not
    worth doing as of yet.

    :param l: int>=0; Order of one Laguerre Polynomial
    :param j: int >=0; Order of the other Laguerre Polynimial
    :param eta: float; the exponent of the extra term left in the integral
    :return: return int_0^infty Ll*Lj*exp(-z)*z^eta dz
    """
    func=lambda x: laguerre(l)(x)*laguerre(j)(x)*np.exp(-x)*x**eta
    result,_ =inte.quad(func,0,np.inf)
    return result

if __name__=="__main__":
    limr=0